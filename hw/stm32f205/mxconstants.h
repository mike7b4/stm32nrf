#define ENABLE_USB 1
#define RF_CSN_PIN GPIO_PIN_4
#define RF_CSN_GPIO_PORT GPIOA
#define RF_CE_PIN GPIO_PIN_0
#define RF_CE_GPIO_PORT GPIOB
#define RF_ISR_GPIO_PORT GPIOB
#define RF_ISR_PIN GPIO_PIN_1
#define DEBUG_GPIO_PORT GPIOB
#define DEBUG_PIN_1 GPIO_PIN_1


#define VCP_TX_GPIO_PORT GPIOA
#define VCP_TX_PIN GPIO_PIN_2
#define VCP_RX_GPIO_PORT GPIOA
#define VCP_RX_PIN GPIO_PIN_3

#define MOSFET_GPIO_PORT GPIOB
#define MOSFET_PIN_1 GPIO_PIN_4
#define MOSFET_PIN_2 GPIO_PIN_5
#define LED_GPIO_PORT GPIOA
#define LED_PIN_1 GPIO_PIN_5

