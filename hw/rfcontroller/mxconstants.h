/* Private define ------------------------------------------------------------*/
/* get rid of this mess replace with struct ASAP!! */

#define ADC_VREF 3000
#define ENABLE_USB (1)
#define ENABLE_NRF (1)
#define ENABLE_ADC (1)
/* was PB1 */
#define RF_CSN_PIN GPIO_PIN_1
#define RF_CSN_GPIO_PORT GPIOB
/* WAS PB6 */
#define RF_CE_PIN GPIO_PIN_6
#define RF_CE_GPIO_PORT GPIOB

#define RF_IRQn EXTI4_15_IRQn
#define RF_ISR_PIN GPIO_PIN_8
#define RF_ISR_GPIO_PORT GPIOA

#define LED_GPIO_PORT_1 GPIOA
#define LED_GPIO_PORT_2 GPIOA
#define LED_PIN_1 GPIO_PIN_5
#define LED_PIN_2 GPIO_PIN_6

#define DEBUG_PIN_1 GPIO_PIN_8
#define DEBUG_GPIO_PORT GPIOA


#define MOSFET_GPIO_PORT_1 GPIOA
#define MOSFET_PIN_1 GPIO_PIN_15

#define MOSFET_GPIO_PORT_2 GPIOB
#define MOSFET_PIN_2 GPIO_PIN_0

#define BATTERY_VOL_PIN GPIO_PIN_0
#define BATTERY_VOL_PORT GPIOA
