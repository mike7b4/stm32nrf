#include "em_queue.h"
#include "em_array.h"
#include "event.h"
#include "measure.h"
#include "command_table.h"
#define BATTERY_SAMPLE_SIZE (32)
#if ENABLE_ADC==1
static EmArray battery_samples;

static bool cb_foreach_battery_sample(uint16_t *adcvalue, uint16_t index, EMSTREAM *p_stream)
{
    em_printf(p_stream, "%d %d\r\n", index, ADC_TO_VOLTAGE(*adcvalue));
    /* keep going */
    return true;
}

static bool on_battery_sample(uint8_t cmd, const uint8_t *args, uint8_t length, EMSTREAM *out)
{
    MeasureADC measure;
    measure_battery_read(&measure);
    if (!em_array_append(&battery_samples, &measure.adcvalue))
    {
        /* remove first inserted and try again */
        em_array_remove(&battery_samples, 0);
        em_array_append(&battery_samples, &measure.adcvalue);
    }
    measure_battery_send(&measure, out);

    return true;
}

static bool on_battery_array(uint8_t cmd, const uint8_t *args, uint8_t length, EMSTREAM *out)
{
    em_array_foreach(&battery_samples, (EmArrayForEachCallback)&cb_foreach_battery_sample, &out);
    return true;
}

void measure_init(void)
{
    static uint16_t _batterysamples[BATTERY_SAMPLE_SIZE];
    em_array_init(&battery_samples, &_batterysamples, BATTERY_SAMPLE_SIZE, sizeof(uint16_t));
	adc_init();
    em_command_register_callback(EVENT_SEND_BATTERY_MEASURE, on_battery_sample);
    em_command_register_callback(EVENT_SEND_BATTERY_ARRAY, on_battery_array);
}

bool measure_battery_read(MeasureADC *p_measure)
{
	HAL_ADCEx_Calibration_Start(&hadc);
	p_measure->calibrate = HAL_ADC_GetValue(&hadc);
	HAL_ADC_Start(&hadc);
    int count = 16;
    uint32_t value = 0;
    while (count--) {
        HAL_ADC_PollForConversion(&hadc, 10);
        value += HAL_ADC_GetValue(&hadc);
    }
    p_measure->adcvalue = value/16;
//	HAL_ADC_Stop(&hadc);
    return true;
}

void measure_battery_send(const MeasureADC *p_measure, EMSTREAM *p_stream)
{
	em_printf(p_stream, "c=%d ", p_measure->calibrate);
    em_printf(p_stream, "ADC=%d ", p_measure->adcvalue);
	/* 3000 * adcvalue/256 */
	em_printf(p_stream, "b=%dv\r\n", ADC_VREF * p_measure->adcvalue >> 8);
}

void measure_current_send_update(EMSTREAM *p_stream)
{
}

void measure_solary_send_update(EMSTREAM *p_stream)
{
}

#endif
