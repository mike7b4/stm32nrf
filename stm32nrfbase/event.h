#ifndef EVENT_H_
#define EVENT_H_
/* to lazy create a linked list so reuse queue a little bit akward */
#include <stddef.h>
#include "em_queue.h"
/* event are requests comming from
 * SENDER below */
typedef enum _EVENT {
	EVENT_IS_EMPTY = 0,
    EVENT_RF_INTERRUPT = 'I',
	EVENT_TOGGLE_FET_1 = '1',
	EVENT_TOGGLE_FET_2 = '2',
	EVENT_SEND_BATTERY_MEASURE = 'b',
	EVENT_SEND_BATTERY_ARRAY = 'B',
	EVENT_SEND_SOLARY_MEASURE = 's',
	EVENT_SEND_CURRENT_MEASURE = 'c',
	EVENT_SEND_PING = 'p',
    EVENT_CLICKED = 'a',
    EVENT_SLEEPMODE_SLEEP = 's',
    EVENT_SLEEPMODE_STOP = 'S'
} EVENT;

/* tells mainloop where to forward response */
typedef enum _SENDER {
	SENDER_USB,
	SENDER_UART,
	SENDER_NRF,
    SENDER_BUTTON1,
    SENDER_BUTTON2,
    SENDER_BUTTON3,
    SENDER_BUTTON4,
	/* systick responses goes to UART or USB */
	SENDER_SYSTICK
} SENDER;

typedef struct _EventObject {
	uint8_t sender;
	uint8_t request;
} EventObject;

void event_init(void);
void event_push(uint8_t sender, uint8_t request);
bool event_pop(EventObject *event);

#endif /* end of include guard: EVENT_H_ */
