#include <stdint.h>
#include "stm32f0xx_hal.h"
#include "rtc.h"
#include "event.h"
#include "gpio.h"
#include "radio_protocol.h"
#include "buttons.h"
#include "command_table.h"
#include "nrf_custom.h"
#define BUTTONS_SIZE 4

/* rfcontrroller we use I2C pins for buttons.
 * This driver should not be compiled when I2C peripherals is used.
 * TBD: ifdef...
 */

#define I2C_DATA_IRQn EXTI4_15_IRQn
#define I2C_CLK_IRQn EXTI4_15_IRQn

typedef struct _Buttons {
    Gpio gpio;
    volatile uint32_t last_ticks;
    volatile bool clicked;
} Buttons;

Buttons buttons[BUTTONS_SIZE] = {
    {
        .gpio =
        {
            .GPIOx = GPIOA,
            .pin = GPIO_PIN_9
        },
        .clicked =  false,
        .last_ticks = 0
    },
    {
        .gpio =
        {
            .GPIOx = GPIOA,
            .pin = GPIO_PIN_10
        },
        .clicked =  false,
        .last_ticks = 0
    },
    {
        .gpio =
        {
            .GPIOx = GPIOA,
            .pin = GPIO_PIN_1
        },
        .clicked =  false,
        .last_ticks = 0
    },
    {
        .gpio =
        {
            .GPIOx = GPIOA,
            .pin = GPIO_PIN_6
        },
        .clicked =  false,
        .last_ticks = 0
    }
};

static bool cb_button_clicked(uint8_t cmd, const uint8_t *args, uint8_t length, EMSTREAM *out)
{
    return buttons_handler(nrf_get(), out);
}

bool buttons_init(void)
{
    for (int i = 0; i < BUTTONS_SIZE; i++) {
        gpio_set_as_input(&buttons[i].gpio, GPIO_PUPDR_PULLUP);
        gpio_enable_isr(&buttons[i].gpio, GPIO_EDGE_FALLING);
    }
    HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
    em_command_register_callback(EVENT_CLICKED, cb_button_clicked);
    return true;
}

bool buttons_handler(Nrf *nrf, EMSTREAM *out)
{
    for (int i = 0; i < BUTTONS_SIZE; i++) {
        if (buttons[i].clicked) {
            buttons[i].clicked = false;
            if (i == 1) {
                radio_protocol_send_set_pwm(nrf, i, 90);
            }
            radio_protocol_send_set_output(nrf, (1 << i), (1 << i), (1 << i));
            em_printf(out, "%02X:clicked\n", i);
        }
    }

    return true;

}

void EXTI0_1_IRQHandler(void)
{
    if ((EXTI->PR & buttons[2].gpio.pin)) {
        EXTI->PR = buttons[2].gpio.pin;
        if (rtc_sec > buttons[2].last_ticks) {
            buttons[2].clicked = true;
            buttons[2].last_ticks = rtc_sec;
            event_push(SENDER_BUTTON3, EVENT_CLICKED);
        }
    }
}

void EXTI4_15_IRQHandler(void)
{
    if ((EXTI->PR & buttons[0].gpio.pin)) {
        EXTI->PR = buttons[0].gpio.pin;
        if (rtc_sec > buttons[0].last_ticks) {
            buttons[0].clicked = true;
            buttons[0].last_ticks = rtc_sec;
            event_push(SENDER_BUTTON1, EVENT_CLICKED);
        }
    }

    if ((EXTI->PR & buttons[1].gpio.pin)) {
        EXTI->PR = buttons[1].gpio.pin;
        if (rtc_sec > buttons[1].last_ticks) {
            buttons[1].clicked = true;
            buttons[1].last_ticks = rtc_sec;
            event_push(SENDER_BUTTON2, EVENT_CLICKED);
        }
    }

    if ((EXTI->PR & buttons[3].gpio.pin)) {
        EXTI->PR = buttons[3].gpio.pin;
        if (rtc_sec > buttons[3].last_ticks) {
            buttons[3].clicked = true;
            buttons[3].last_ticks = rtc_sec;
            event_push(SENDER_BUTTON4, EVENT_CLICKED);
        }
    }

}
