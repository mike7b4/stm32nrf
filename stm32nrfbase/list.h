#ifndef LIST_H_
#define LIST_H_
#include <stdint.h>
typedef struct _List List;
struct _List {
	List *p_next;
	void *p_data;
};

List* list_init(List *list);
List* list_link(List *list, List *insertme);
List* list_unlink(List *list, List *remove);

#endif /* end of include guard: LIST_H_ */
