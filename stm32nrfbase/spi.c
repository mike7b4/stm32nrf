#include "spi.h"
#include "gpio.h"

SPI_HandleTypeDef hspi1;

Gpio gpio_spi_SCK = {
    .GPIOx = GPIOB,
    .pin = GPIO_PIN_3,
};

Gpio gpio_spi_MISO = {
    .GPIOx = GPIOB,
    .pin = GPIO_PIN_4,
};

Gpio gpio_spi_MOSI = {
    .GPIOx = GPIOB,
    .pin = GPIO_PIN_5,
};

/* SPI1 init function */
void spi_master_init(void)
{
    hspi1.Instance = SPI1;

    hspi1.Init.Mode = SPI_MODE_MASTER;
    hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
    hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
    hspi1.Init.NSS = SPI_NSS_SOFT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi1.Init.CRCPolynomial = 0;
    hspi1.Init.TIMode = 0;
    /* Peripheral clock enable */
    __HAL_RCC_SPI1_CLK_ENABLE();

    gpio_set_as_alternate_output(&gpio_spi_SCK, GPIO_OTYPER_PP, GPIO_AFSEL_AF0, false);
    gpio_set_as_alternate_output(&gpio_spi_MOSI, GPIO_OTYPER_PP, GPIO_AFSEL_AF0, false);
    gpio_set_as_alternate_input(&gpio_spi_MISO, GPIO_PUPDR_NONE, GPIO_AFSEL_AF0);

    /* 1edge, CLK low idle, Motorola Mode, internal slave, NO internal slave managment */
    SPI1->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM;
    /* 8bit */
    SPI1->CR2 = SPI_CR2_SSOE | (0x700);
}

void spi_deinit(void)
{
    /* Peripheral clock disable */
    __HAL_RCC_SPI1_CLK_DISABLE();

    gpio_set_as_input(&gpio_spi_SCK, GPIO_PUPDR_NONE);
    gpio_set_as_input(&gpio_spi_MOSI, GPIO_PUPDR_NONE);
}

