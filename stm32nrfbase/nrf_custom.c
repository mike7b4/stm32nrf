#include <stdbool.h>
#include <string.h>
#include "spi.h"
#include "hw_config.h"
#include "config.h"
#include "mosfet.h"
#include "nrf.h"
#include "led.h"
#include "nrf_custom.h"
#include "command_table.h"
#include "radio_protocol.h"

static bool cb_spi_txrx(uint8_t *tx, uint8_t *rx, uint8_t length, SPI_HandleTypeDef *hspi);
static void cb_enable_radio(bool enable);
static void cb_delay_us(uint16_t us);
static bool cb_nrf_putbyte(EMSTREAM *p_stream, uint8_t byte);
extern SPI_HandleTypeDef hspi1;
static Nrf nrf = {
    .spi_transmit_receive =(nrf_spi_transmit_receive)cb_spi_txrx,
    .delay_us = cb_delay_us,
    .enable_radio = cb_enable_radio,
    .p_data = &hspi1
};


EMSTREAM nrfstream = {
    .put_byte = cb_nrf_putbyte,
};

uint8_t address[5] = {0x05, 0x20, 0x20, 0x30, 0x01};
Gpio gpio_ESP_UART_RX = {
    .GPIOx = GPIOA,
    .pin = GPIO_PIN_9,
};

Gpio gpio_CE = {
    .GPIOx = GPIOA,
    .pin = GPIO_PIN_2
};

Gpio gpio_CSN = {
    .GPIOx = GPIOA,
    .pin = GPIO_PIN_4
};

Gpio gpio_IRQ = {
    .GPIOx = GPIOA,
    .pin = GPIO_PIN_3,
};
#define RF_IRQn EXTI2_3_IRQn

/* Notice!!!! Overidded by radio_protocol!!! */
static bool cb_nrf_putbyte(EMSTREAM *p_stream, uint8_t byte)
{
    bool ok = true;
    static uint8_t tx_data[8] = {0,};
    static uint8_t tx_index = 0;

    switch (byte) {
    case '\n':
    case '\r':
    case '\0': {
        /* We are done let's send but only if there was data to send. */
        if (tx_index > 0) {
            tx_index = sizeof(tx_data);
        }
        break;
    }
    default:
        tx_data[tx_index++] = byte;
    }

    if (tx_index == sizeof(tx_data)) {
        nrf_enable_tx(&nrf);
        if (!nrf_transmit_data(&nrf, tx_data, tx_index)) {
            ok = false;
        }

        /* Flush buffer even if fail. */
        memset(tx_data, 0, sizeof(tx_data));
        tx_index = 0;
    }

    return ok;
}

static bool cb_spi_txrx(uint8_t *tx, uint8_t *rx, uint8_t length, SPI_HandleTypeDef *hspi)
{
    int res = HAL_SPI_ERROR_NONE;
    gpio_write_pin(&gpio_CSN, false);
    res = HAL_SPI_TransmitReceive(hspi, tx, rx, length, 10);
    gpio_write_pin(&gpio_CSN, true);

    if (res != HAL_OK)
        em_printf(&out, "SPI error %d\r\n", res);

    return res == HAL_OK ? true : false;
}

static bool cb_rf_interrupt(uint8_t cmd, const uint8_t *args, uint8_t length, EMSTREAM *out)
{
    nrf_interrupt_handler(&nrf);
    //em_printf(out, "RF interrupt %X\n", nrf.status);
    if (nrf.status == 0x40) {
        radio_protocol_process_incoming(&nrf, out);
    }
    return true;
}

static void cb_delay_us(uint16_t us)
{
    stm_usleep(us);
}

static void cb_enable_radio(bool enable)
{
    gpio_write_pin(&gpio_CE, enable);
}

void EXTI2_3_IRQHandler(void)
{
    EXTI->PR = gpio_IRQ.pin;
    event_push(SENDER_NRF, EVENT_RF_INTERRUPT);
}

Nrf* nrf_get(void)
{
    return &nrf;
}

bool nrf_platform_setup(void)
{
    spi_master_init();
    gpio_set_as_output(&gpio_CE, GPIO_OTYPER_PP, false);
    gpio_set_as_output(&gpio_CSN, GPIO_OTYPER_PP, true);
    gpio_set_as_input(&gpio_IRQ, GPIO_PUPDR_NONE);
    gpio_set_as_input(&gpio_ESP_UART_RX, GPIO_PUPDR_NONE);
    gpio_enable_isr(&gpio_IRQ, GPIO_EDGE_FALLING);
    NVIC_EnableIRQ(RF_IRQn);

    radio_protocol_init(&nrf, &nrfstream);
    em_command_register_callback('I', cb_rf_interrupt);

    nrf_set_datarate(&nrf, NRF_RF_SETUP_DR_2Mbps);
    nrf_set_power_dBm(&nrf, NRF_RF_SETUP_PWR_0dBm);
    address[4] = config_tx_address();
    nrf_set_tx_address(&nrf, address);
    address[4] = config_rx_address();
    nrf_set_rx_address(&nrf, NRF_PIPE0, address, 8, true);
    /* If pin high set it in rxmode */
    if (config_rf_on()) {
        mosfet_set(0, true);
        stm_msleep(200);
        mosfet_set(0, false);
        return nrf_enable_rx(&nrf);
    }

    return true;
}

bool nrf_custom_handler(EventObject *event)
{
    switch (event->request) {
    case 'r':
        em_printf(&out, "RX %s\r\n", nrf_enable_rx(&nrf) ? "OK" : "ERROR");
        break;
    case 't':
        if (nrf_enable_tx(&nrf)) {
            em_printf(&nrfstream, "HELLO WORLD :)\n");
            em_printf(&out, "TX OK\n");
        } else {
            em_printf(&out, "TX ERROR\n");
        }
        break;
    case 'd':
        nrf_dump_registers(&nrf, &out);
        em_printf(&out, "RF_ISR_PIN is: %X\n", gpio_read_pin(&gpio_IRQ));
        break;
    case 'T':
        nrf_power_down(&nrf);
        break ;
    case 'I':
        nrf_interrupt_handler(&nrf);
        em_printf(&out, "RF interrupt %X\n", nrf.status);
        if (nrf.status == 0x40) {
            radio_protocol_process_incoming(&nrf, &out);
        }
        break;
    case 'n':
        em_printf(&nrfstream, "HELLO\n");
        break ;
    default:
        return false;
    }

    return true;
}

