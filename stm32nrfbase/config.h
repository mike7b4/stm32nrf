#ifndef _CONFIG_H__
#define _CONFIG_H__
#include <stdint.h>
#include <stdbool.h>
#define CONFIG_PAGE (31)
bool config_init(void);
bool config_rf_on(void);
bool config_rx_set_address(uint8_t addr);
bool config_tx_set_address(uint8_t addr);
bool config_usb_enabled(void);
uint8_t config_rx_address(void);
uint8_t config_tx_address(void);


#endif /* End of _CONFIG_H__ */
