#ifndef LED_H_
#define LED_H_

void led_init(void);
bool led_set(uint8_t id, bool onoff);
bool led_toggle(uint8_t id);

#endif /* end of LED_H_ */
