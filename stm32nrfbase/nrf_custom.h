#ifndef _NRF_CUSTOM_H_
#define _NRF_CUSTOM_H_
#include "nrf.h"
#include "event.h"
bool nrf_platform_setup(void);
bool nrf_custom_handler(EventObject *p_event);
Nrf* nrf_get(void);
extern EMSTREAM nrfstream;

#endif /* _NRF_CUSTOM_H_ end of _NRF_CUSTOM_H_ */
