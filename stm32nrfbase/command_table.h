#ifndef _COMMAND_TABLE_H_
#define _COMMAND_TABLE_H_
#include "em_printf.h"
typedef bool (*EmCommandCallback)(uint8_t cmd, const uint8_t *arguments, uint8_t argument_length, EMSTREAM *p_sender);
bool em_command_register_callback(uint8_t cmd, EmCommandCallback callback);

bool em_commands_iterate_buffer(const uint8_t *inbuffer, uint8_t length, EMSTREAM *p_stream);
static inline bool em_commands_iterate_command(const uint8_t cmd, EMSTREAM *p_stream)
{
    return em_commands_iterate_buffer(&cmd, 1, p_stream);
}

#endif /* _COMMAND_TABLE_H_ */

