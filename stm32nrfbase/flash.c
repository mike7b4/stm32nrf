#ifndef UNITTEST
#include "hw_config.h"
#endif
#include <string.h>
#include <stddef.h>
#include "flash.h"

#define FLASH_UNLOCK() \
{\
    while ((FLASH->SR & FLASH_SR_BSY) != 0) { asm volatile("nop"); } \
    if ((FLASH->CR & FLASH_CR_LOCK) != 0) { \
        FLASH->KEYR = FLASH_KEY1; \
        FLASH->KEYR = FLASH_KEY2; \
    } \
}\
/* End makro */

#define FLASH_LOCK() \
{\
    while ((FLASH->SR & FLASH_SR_BSY) != 0) { asm volatile("nop"); } \
    FLASH->CR |= FLASH_CR_LOCK; \
}\
/* End makro */

typedef struct _FlashPage {
    uint8_t buffer[FLASH_PAGE_SIZE];
    uint8_t page;
    uint16_t buf_index;
    uint8_t reserved;
} FlashPage  __attribute__ ((aligned(16)));

static FlashPage _flash = {0,};

bool flash_init(void)
{
    memset(&_flash.buffer, 0xFF, FLASH_PAGE_SIZE);
    _flash.buf_index = 0;
    _flash.page = 0xFF;
    return true;
}

bool flash_set_page(uint8_t page)
{
    if (page != 0xFF && page > 31)
        return false;

    _flash.page = page;
    return true;
}

bool flash_push_bytes(const uint8_t *bytes, uint16_t size)
{
    if (bytes == NULL || size > FLASH_PAGE_SIZE - _flash.buf_index)
        return false;

    uint8_t *bufptr = _flash.buffer + _flash.buf_index;
    const uint8_t *sptr = bytes;
    for (; size > 0; _flash.buf_index++, size--) {
        *bufptr = *sptr;
        bufptr++;
        sptr++;
    }

    return true;
}

bool flash_execute(void)
{
    if (_flash.page > 31)
        return false;

#ifndef UNITTEST
    FLASH_UNLOCK();
    // Do flash erase
    FLASH->CR |= FLASH_CR_PER;
    FLASH->AR =  0x08000000 + (FLASH_PAGE_SIZE * _flash.page);
    FLASH->CR |= FLASH_CR_STRT;
    while ((FLASH->SR & FLASH_SR_BSY) != 0) { asm volatile("nop"); }

    if ((FLASH->SR & FLASH_SR_EOP) == 0) {
        // Erase failed
        FLASH->CR &= ~FLASH_CR_PER;
        FLASH_LOCK();
        return false;
    } else {
        FLASH->SR = FLASH_SR_EOP;
    }

    FLASH->CR &= ~FLASH_CR_PER;

    if (_flash.buf_index == 0) {
        goto unlock;
    }
    // Program
    uint16_t pagestart = FLASH_PAGE_SIZE * _flash.page;
    // Must be multiply by 16
    uint16_t pageend = (_flash.buf_index % 2) == 0 ? (pagestart + _flash.buf_index) : (pagestart + _flash.buf_index + 1);
    uint16_t *bufptr = (uint16_t*)(_flash.buffer);
    for (uint16_t pageoffset = pagestart; pageoffset < pageend; pageoffset+= 2) {
        FLASH->CR |= FLASH_CR_PG;
        *(__IO uint16_t *)(0x08000000 + pageoffset) = *bufptr;
        while ((FLASH->SR & FLASH_SR_BSY) != 0) { asm volatile("nop"); }

        if ((FLASH->SR & FLASH_SR_EOP) == 0) {
            // Flash failed
            FLASH->CR &= ~FLASH_CR_PG;
            FLASH_LOCK();
            return false;
        } else {
            FLASH->SR = FLASH_SR_EOP;
        }
        bufptr++;
    }
    FLASH->CR &= ~FLASH_CR_PG;

unlock:
    FLASH_LOCK();
    return true;
#endif
    return false;
}

#ifdef UNITTEST
#include "../../libembedded/tests/test.h"
int main(int argc, char **argv)
{
    uint8_t empty[FLASH_PAGE_SIZE];
    uint8_t bytes[8];
    memset(&empty, 0xFF, sizeof(empty));
    memset(&bytes, 0xFF, sizeof(bytes));
    memcpy(bytes, "HELLO\n", sizeof("HELLO\n"));
    EXPECT(flash_init(), true, "flash_init");
    EXPECT(_flash.page, 0xFF, "page should be 'not set' (0xFF)");

    EXPECT(flash_set_page(0), true, "set_page should success");
    EXPECT(_flash.page, 0, "page should set to 0");

    EXPECT(flash_set_page(80), false, "set_page should fail");
    EXPECT(_flash.page, 0, "page should still be 0");

    EXPECT(flash_set_page(0xFF), true, "unset_page should success");
    EXPECT(_flash.page, 0xFF, "page should set to 0xFF");

    EXPECT(memcmp(_flash.buffer, empty, FLASH_PAGE_SIZE), 0, "whole should be initialized with 0xFF");
    EXPECT(flash_push_bytes(bytes, 8), true, "put_bytes");
    EXPECT(_flash.buf_index, 8, "Index should be 8");
    EXPECT(memcmp(_flash.buffer, "HELLO\n", sizeof("HELLO\n")), 0, "Flash should have HELLO at start");
    EXPECT(memcmp(_flash.buffer + sizeof("HELLO\n"), empty, FLASH_PAGE_SIZE - sizeof("HELLO\n")), 0, "Flash should have 0xFF after HELLO");

    EXPECT(flash_push_bytes(bytes, 8), true, "push_bytes");
    EXPECT(_flash.buf_index, 16, "Index should be 16");
    EXPECT(memcmp(_flash.buffer, "HELLO\n\0\xFFHELLO\n\0\xFF", 16), 0, "Flash should have HELLO\\n\\0\\FF twice");
 
    EXPECT(flash_init(), true, "flash_init to reset state");
    EXPECT(memcmp(_flash.buffer, empty, FLASH_PAGE_SIZE), 0, "The whole buffer should be empty (0xFF)");

    uint8_t b1025[FLASH_PAGE_SIZE + 1] = {0,};
    EXPECT(flash_push_bytes(b1025, sizeof(b1025)), false, "push_bytes should fail");
    EXPECT(flash_push_bytes(b1025, 512), true, "put_bytes 512 should success");
    EXPECT(flash_push_bytes(b1025, 512), true, "put_bytes 512 should success");
    EXPECT(_flash.buf_index, 1024, "Index should be 1024");
    EXPECT(flash_push_bytes(b1025, 1), false, "put one_byte should fail");
    return 0;
}

#endif
