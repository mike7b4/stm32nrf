#ifndef MEASURE_H
#define MEASURE_H

#include "em_printf.h"
#include "hw_config.h"
#define ADC_TO_VOLTAGE(adc) \
    (ADC_VREF * (adc) >> 8) \
    /* end macro */

typedef struct _MeasureADC {
    uint16_t calibrate;
    uint16_t adcvalue;
} MeasureADC;

void measure_init(void);
bool measure_battery_read(MeasureADC *p_measure);
void measure_battery_send(const MeasureADC *p_measure, EMSTREAM *p_stream);
void measure_current_send(EMSTREAM *p_stream);
void measure_solary_send(EMSTREAM *p_stream);


#endif
