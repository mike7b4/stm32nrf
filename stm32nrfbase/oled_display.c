#include "i2c.h"
#include "oled_display.h"

void ssd_command(uint8_t c)
{
    uint8_t b[] = { 0, c };
    i2c_write(0x78, b, 2);
}

void ssd_command_with_arg(uint8_t c, uint8_t arg)
{
    uint8_t b[] = { 0, c, 0 , arg };
    i2c_write(0x78, b, sizeof(b));
}

void ssd_command_with_two_args(uint8_t c, uint8_t arg, uint8_t arg2)
{
    uint8_t b[] = { 0, c, 0 , arg, 0 , arg2};
    i2c_write(0x78, b, sizeof(b));
}

void oled_display_init(void)
{
    uint8_t b[] = { 0x00, 0xAE,
                            0x84, 0xFF, 0x1F, 0x8F , 0xFF, 0xFF , 0xFF, 0xFF,
                            0xFF, 0x00, 0xFF, 0x80 , 0x00, 0xFF , 0xFF, 0xFF };

    i2c_init();

    ssd_command(0xA5);
    /* CLOCKDIV */
    ssd_command_with_arg(0xD5, 0x80);

    /* set multiplex */
    ssd_command_with_arg(0xA8, 0x1F);

    /* SETDISPLAY OFFSET */
    ssd_command_with_arg(0xD3, 0x00);


    /* set disp strt line */
    ssd_command(0x40);

    /* charge pump */
    ssd_command_with_arg(0x8D, 0x14);

    /* page addressing mode */
    ssd_command_with_arg(0x20, 0x00);

    /* segremap C8 | 1 */
    ssd_command(0xA1);
    /* snan dir reverse */
    ssd_command(0xC8);

    ssd_command_with_arg(0xDA, 0x02);
    /* contrast */
    ssd_command_with_arg(0x81, 0x8F);
    /* pre chrge period */
    ssd_command_with_arg(0xD9, 0xF1);
    /* vcom deselect */
    ssd_command_with_arg(0xDB, 0x40);

    b[0] = 0x40;
    i2c_write(0x78, b, 17);

    /* deactivte scroll */
    ssd_command(0x2E);
}

