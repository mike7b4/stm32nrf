#ifndef MOSFET_H_
#define MOSFET_H_
#include "em_printf.h"
#include "hw_config.h"

typedef enum _Switches {
    LOAD_SW1 = 0,
    LOAD_SW2,
    LOAD_SW3,
    LOAD_SW4
} Switches;
void mosfet_init(void);
bool mosfet_set(uint8_t id, bool onoff);
bool mosfet_get(uint8_t id);
bool mosfet_toggle(uint8_t id, uint8_t dutycycle);
bool mosfet_pwm_off(uint8_t id);
bool mosfet_pwm_on(uint8_t id, uint8_t dutycycle);
void mosfet_send_state(EMSTREAM *p_stream);

#endif /* end of include guard: POWER_H_ */
