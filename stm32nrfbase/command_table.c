
#include "command_table.h"
#define COMMAND_SIZE (16)

typedef struct {
    EmCommandCallback callback;
    uint8_t cmd;
} PrivCommand;

PrivCommand _commands[COMMAND_SIZE] = {{NULL, 0},};
static uint8_t _command_count = 0;

bool em_command_register_callback(uint8_t cmd, EmCommandCallback callback)
{
    if (_command_count == COMMAND_SIZE)
        return false;

    _commands[_command_count].callback = callback;
    _commands[_command_count].cmd = cmd;
    _command_count++;
    return true;
}

bool em_commands_iterate_buffer(const uint8_t *inbuffer, uint8_t length, EMSTREAM *p_sender)
{
    if (length == 0)
        return false;

    length--;
    uint8_t cmd = inbuffer[0];
    const uint8_t *args = (length > 0) ? &inbuffer[1] : NULL;
    for (int i = 0;i < _command_count; i++) {
        /* if callback return true we are done */
        if (cmd == _commands[i].cmd && _commands[i].callback(cmd, args, length, p_sender))
            return true;
    }

    return false;
}


