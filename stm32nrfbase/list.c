#include <stddef.h>
#include "list.h"

List* list_init(List *list)
{
    list->p_next = NULL;
    return list;
}

List *list_link(List *head, List *insert)
{
    List *current = head;

    while (current->p_next != NULL) {
        current = current->p_next;
    }

    current->p_next = insert;

    return head;
}

List *list_unlink(List *head, List *remove)
{
    List *current = head;
    List *prev = NULL;

    while (current != remove) {
        prev = current;
        current = current->p_next;
    }

    if (current != remove)
        return head;

    if (prev) prev->p_next = current->p_next;

    return head == current ? current->p_next : head;
}

