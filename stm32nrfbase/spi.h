#ifndef __spi_H
#define __spi_H
#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"

extern SPI_HandleTypeDef hspi1;

void spi_master_init(void);
void spi_deinit(void);

#ifdef __cplusplus
}
#endif
#endif /*__ spi_H */

