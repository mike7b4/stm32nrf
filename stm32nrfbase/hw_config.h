#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "mxconstants.h"
#if defined (STM32F042x6)
#include "stm32f0xx_hal.h"
#elif defined (STM32F103x6)
#include "stm32f1xx_hal.h"
#elif defined (STM32F205xx)
#include "stm32f2xx_hal.h"
#elif defined (STM32F303x8)
#include "stm32f3xx_hal.h"
#elif defined (STM32L476xx)
#include "stm32l4xx_hal.h"
#endif
#include "em_printf.h"
#include "stm_delay.h"
#include "gpio.h"
#include "adc.h"

extern EmStream out;
