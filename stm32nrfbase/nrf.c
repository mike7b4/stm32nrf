#include <string.h>
#include "hw_config.h"
#include "spi.h"
#include "event.h"
#include "nrf.h"


void nrf_init(Nrf *nrf)
{
    nrf->enable_radio(false);
    nrf_power_down(nrf);
    nrf_rx_flush(nrf);
    nrf_tx_flush(nrf);
    nrf_get_register(nrf, NRF_REG_STATUS, &nrf->status);
}

bool nrf_set_register(Nrf *nrf, uint8_t reg, uint8_t value)
{
    bool res = false;
    nrf->tx_buffer[0] = NRF_W_REGISTER | reg;
    nrf->tx_buffer[1] = value;
    if (nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 2, nrf->p_data)) {
        nrf->status = nrf->rx_buffer[0];
        res = true;
    }

    return res;
}

bool nrf_get_register(Nrf *nrf, uint8_t reg, uint8_t *value)
{
    bool res = false;
    nrf->tx_buffer[0] = NRF_R_REGISTER | reg;
    nrf->tx_buffer[1] = 0xFF;
    if (nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 2, nrf->p_data) == true) {
        /* status byte always received at pos 0 */
        nrf->status = nrf->rx_buffer[0];
        if (value != NULL) {
            *value = nrf->rx_buffer[1];
        }
        res = true;
    }

    return res;
}

static void nrf_fetch_rx_package(Nrf *nrf, uint8_t id)
{
    nrf->tx_buffer[0] = NRF_R_RX_PAYLOAD;
    for (int i = 0; i < 8; i++) {
        nrf->tx_buffer[i+1] = 0xFF;
    }
    if (nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_pipe, 9, nrf->p_data) == true) {
    }
}

void nrf_interrupt_handler(Nrf *nrf)
{
    nrf_get_register(nrf, NRF_REG_STATUS, &nrf->status);
    if (nrf->status & 0x70) {
        nrf_set_register(nrf, NRF_REG_STATUS, (nrf->status & 0x70));
    }

  //  event_push(SENDER_NRF, EVENT_RF_INTERRUPT);
    switch (nrf->status & 0x70) {
    case NRF_STATUS_RX_DR:
        nrf_fetch_rx_package(nrf, 0);
        break;
    case NRF_STATUS_TX_DR:
        break;
    case NRF_STATUS_MAX_RT:
        break;
    }
}

bool nrf_power_down(Nrf *nrf)
{
    nrf->enable_radio(false);
    /* make sure power down and mask ISR */
    nrf_set_register(nrf, NRF_REG_CONFIG, 0x70);
    nrf->delay_us(5);
    /* make sure no pending ISR */
    if (nrf->status & 0x70) {
        nrf_set_register(nrf, NRF_REG_STATUS, nrf->status & 0x70);
    }
    /* make sure power down */
    nrf_get_register(nrf, NRF_REG_CONFIG, &nrf->config);
    nrf_rx_flush(nrf);
    nrf_tx_flush(nrf);

    return true;
}

bool nrf_set_tx_address(Nrf *nrf, const uint8_t address[5])
{
    nrf->tx_buffer[0] = NRF_REG_TX_ADDR | NRF_W_REGISTER;
    for (int i = 0; i < 5; i++) {
        nrf->tx_buffer[i+1] = address[i];
    }

    return nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 6, nrf->p_data);
}

bool nrf_enable_tx(Nrf *nrf)
{
    nrf_power_down(nrf);
    /* enable TX_DR and POWER UP and set to TX isr */
    nrf_set_register(nrf, NRF_REG_CONFIG, NRF_CONFIG_MASK_RX_DR | NRF_CONFIG_EN_CRC | NRF_CONFIG_PWR_UP);
    return nrf_get_register(nrf, NRF_REG_CONFIG, &nrf->config);
}

bool nrf_set_channel(Nrf *nrf, uint8_t channel)
{
    return nrf_set_register(nrf, NRF_REG_RF_CH, channel & 0x7F);
}

bool nrf_set_power_dBm(Nrf *nrf, uint8_t pwr)
{
    uint8_t tmp = 0;
    nrf_get_register(nrf, NRF_REG_RF_SETUP, &tmp);
    tmp &= ~0x06;
    tmp = tmp | (pwr & 0x06);
    nrf_set_register(nrf, NRF_REG_RF_SETUP, tmp);
    return nrf_get_register(nrf, NRF_REG_RF_SETUP, &nrf->rf_setup);
}

bool nrf_set_datarate(Nrf *nrf, uint8_t rate)
{
    uint8_t tmp = 0;
    nrf_get_register(nrf, NRF_REG_RF_SETUP, &tmp);
    tmp &= ~0x28;
    tmp = tmp | (rate & 0x28);
    nrf_set_register(nrf, NRF_REG_RF_SETUP, tmp);
    return nrf_get_register(nrf, NRF_REG_RF_SETUP, &nrf->rf_setup);
}

bool nrf_set_rx_address(Nrf *nrf, uint8_t pipe, const uint8_t address[5], uint8_t payload_width, bool enable_auto_ack)
{
    if (pipe >= 6)
        return false;

    if (payload_width > 32)
        return false;

    nrf->tx_buffer[0] = NRF_W_REGISTER | (NRF_REG_RX_ADDR_P0 + pipe);
    for (int b = 0; b < 5; b++) {
        nrf->tx_buffer[b+1] = address[b];
    }
    nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 6, nrf->p_data);

    nrf_set_register(nrf, NRF_REG_RX_PW_P0 + pipe, payload_width);

    if (enable_auto_ack) {
        uint8_t tmp = 0;
        nrf_get_register(nrf, NRF_REG_EN_AA, &tmp);
        nrf_set_register(nrf, NRF_REG_EN_AA, tmp | (1 << pipe));
    }

    return true;
}


bool nrf_enable_rx(Nrf *nrf)
{
    nrf_power_down(nrf);

    /* address length 5 */
    nrf_set_register(nrf, NRF_REG_SETUP_AW, 3);
    /* enable RX_DR and POWER UP and set to RX */
    nrf_set_register(nrf, NRF_REG_CONFIG, NRF_CONFIG_EN_CRC | NRF_CONFIG_PRIM_RX | NRF_CONFIG_MASK_TX_DS | NRF_CONFIG_MASK_MAX_RT | NRF_CONFIG_PWR_UP);

    nrf_get_register(nrf, NRF_REG_CONFIG, &nrf->config);
    nrf->enable_radio(true);
    return true;
}

bool nrf_transmit_data(Nrf *nrf, uint8_t *data, uint8_t length)
{
    if (length > NRF_TX_BUFFER_SIZE - 1 || (nrf->config & NRF_CONFIG_PRIM_RX))
        return false;

    nrf->tx_buffer[0] = NRF_W_TX_PAYLOAD;
    for (int i = 0; i < length; i++) {
        nrf->tx_buffer[i+1] = data[i];
    }

    nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, length+1, nrf->p_data);
    nrf->enable_radio(true);
    stm_msleep(20);
  //  nrf->delay_us(2000);
    nrf->enable_radio(false);
    return true;
}

bool nrf_tx_flush(Nrf *nrf)
{
    nrf->tx_buffer[0] = NRF_FLUSH_TX;
    nrf->tx_buffer[1] = 0xFF;
    nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 1, nrf->p_data);
    return true;
}

bool nrf_rx_flush(Nrf *nrf)
{
    nrf->tx_buffer[0] = NRF_FLUSH_RX;
    nrf->tx_buffer[1] = 0xFF;
    nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 1, nrf->p_data);
    return true;
}

uint8_t nrf_get_fifo_status(Nrf *nrf)
{
    uint8_t value = 0;
    nrf_get_register(nrf, NRF_REG_FIFO_STATUS, &value);
    return value;
}

void nrf_dump_registers(Nrf *nrf, EMSTREAM *em)
{
    for (int i = 0; i <= 0x07; i++) {
        uint8_t tmp = 0;
        if (nrf_get_register(nrf, i, &tmp)) {
            em_printf(em, "Register: %0X Value %0X\r\n", i, tmp);
        } else {
            em_printf(em, "Register read ERROR\r\n");
        }
    }

    memset(nrf->tx_buffer, 0xFF, 6);
    nrf->tx_buffer[0] = NRF_REG_RX_ADDR_P0;
    nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 6, nrf->p_data);
    em_printf(em, "RXAddress: %H\r\n", &nrf->rx_buffer[1], 5);
    nrf->tx_buffer[0] = NRF_REG_TX_ADDR;
    memset(nrf->rx_buffer, 0xFF, 6);
    nrf->spi_transmit_receive(nrf->tx_buffer, nrf->rx_buffer, 6, nrf->p_data);
    em_printf(em, "TXAddress: %H\r\n", &nrf->rx_buffer[1], 5);
}

