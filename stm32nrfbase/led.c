#include "gpio.h"
#include "led.h"

static Gpio gpio_leds[] = {
    {
        .GPIOx = GPIOA,
        .pin = GPIO_PIN_5
    }
};

#define LEDS_COUNT 1

void led_init(void)
{
    gpio_set_as_output(&gpio_leds[0], GPIO_OTYPER_PP, false);
}

bool led_set(uint8_t id, bool onoff)
{
    if (id > LEDS_COUNT)
        return false;

    gpio_write_pin(&gpio_leds[id], onoff);
    return true;
}

bool led_toggle(uint8_t id)
{
    if (id > LEDS_COUNT)
        return false;

    gpio_toggle_pin(&gpio_leds[id]);
    return true;
}

