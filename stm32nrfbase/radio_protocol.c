#include <string.h>
#include "nrf.h"
#include "mosfet.h"
#include "radio_protocol.h"

typedef struct _EmRPStream {
    uint8_t index;
    /* Make sure 32 bit aligned. */
    uint8_t pad[3];
    RPText rp_text;
    Nrf *nrf;
} EmRPStream;

RP_PWM g_pwms = {
    .msg = 0,
    .sender_id = 0,
    .mask = 0xF,
    .pwms = {100, 100, 100, 100},
    .reserved = 0x0
};

/* Only used when send TEXT */
static bool cb_rp_put_byte(EMSTREAM *p_stream, uint8_t byte)
{
    bool ok = false;
    /* Yeye dirty cast, but atleast I do some sanity checks. */
    EmRPStream *rpc = (EmRPStream *)(p_stream->p_data);
    if (rpc == NULL || rpc->nrf == NULL || rpc->index >= MSG_BODY_SIZE)
        return false;

    if (rpc->index == 0) {
        /* header.sender_id should already have been filled on upstart or
         * when paired. So we set index to 2 and start fill from there. */
    }

    switch (byte) {
    case '\0':
    case '\n':
    case '\r':
        rpc->rp_text.body[rpc->index] = byte;
        /* Set index to 6 so that we send. */
        rpc->index = MSG_BODY_SIZE;
        break;
    default:
        rpc->rp_text.body[rpc->index++] = byte;
        break;
    }

    if (rpc->index == MSG_BODY_SIZE) {
        rpc->rp_text.msg = RP_MSG_TEXT;
        nrf_enable_tx(rpc->nrf);
        if (!nrf_transmit_data(rpc->nrf, (uint8_t *)&rpc->rp_text, sizeof(RPText))) {
            ok = false;
        }

        /* Flush buffer even if fail. */
        rpc->rp_text.msg = RP_MSG_TEXT;
        memset(rpc->rp_text.body, 0, MSG_BODY_SIZE);
        rpc->index = 0;
    }

    return ok;
}

bool radio_protocol_init(Nrf *nrf, EMSTREAM *p_stream)
{
    static EmRPStream rpc = {0,};
    if (p_stream == NULL || nrf == NULL)
        return false;

    rpc.nrf = nrf;
    p_stream->p_data = &rpc;
    p_stream->put_byte = cb_rp_put_byte;

    return true;
}

bool radio_protocol_process_incoming(Nrf *nrf, EMSTREAM *outstream)
{
    if (nrf == NULL)
        return false;

    /* Raise CONDITION WARNING FIXME
     * rf should be reactive again until incoming has been processed!!! */
    uint8_t msg = nrf->rx_pipe[1];
    switch (msg) {
    case RP_MSG_TEXT: {
        RPText *rpt = (RPText*)&(nrf->rx_pipe[1]);
        em_printf(outstream, "%02X:%s", rpt->sender_id, rpt->body);
        memset(&nrf->rx_pipe[1], 0, sizeof(nrf->rx_pipe) - 1);
        break;
    }
    case RP_MSG_SET_OUTPUT: {
        RP_Output *rpsw = (RP_Output*)&(nrf->rx_pipe[1]);
        for (int mask = 0x01, index = 0; mask <= 0x08; mask <<= 1, index++) {
            if (rpsw->mask & mask) {
                if (rpsw->toggle & mask) {
                    mosfet_toggle(index, g_pwms.pwms[index]);
                } else if ((rpsw->set & mask)) {
                    mosfet_pwm_on(index, g_pwms.pwms[index]);
                } else {
                    mosfet_pwm_off(index);
                }
            }
        }
        break;
    }
    case RP_MSG_SET_PWM: {
        RP_PWM *rpsw = (RP_PWM*)&(nrf->rx_pipe[1]);
        /* Only 4 bits!!! */
        for (int mask = 0x01, index = 0; mask <= 0x08; mask <<= 1, index++) {
            if (rpsw->mask & mask) {
                //mosfet_pwm_on(index, rpsw->pwms[index]);
                g_pwms.pwms[index] = rpsw->pwms[index];
            }
        }
        break;
     }
     default:
        return false;
    }

    return true;
}

static uint8_t send_count = 0;
bool radio_protocol_send_set_output(Nrf *nrf, uint8_t switches_mask, uint8_t switches_set, uint8_t switches_toggle)
{
    send_count++;
    RP_Output rpsw = {0,};
    rpsw.msg = RP_MSG_SET_OUTPUT;
    rpsw.sender_id = 0xFF;
    rpsw.mask = switches_mask;
    rpsw.set = switches_set;
    rpsw.toggle = switches_toggle;
    nrf_enable_tx(nrf);
    return nrf_transmit_data(nrf, (uint8_t *)&rpsw, sizeof(RP_Output));
}

bool radio_protocol_send_set_pwm(Nrf *nrf, uint8_t index, uint8_t dutycycle)
{
    if (index >= 4)
        return false;

    send_count++;
    RP_PWM rpsw = {0,};
    rpsw.msg = RP_MSG_SET_PWM;
    rpsw.sender_id = 0xFF;
    rpsw.mask = (1 << index);
    rpsw.pwms[index] = dutycycle;
    nrf_enable_tx(nrf);
    return nrf_transmit_data(nrf, (uint8_t *)&rpsw, sizeof(RP_Output));
}

