#include <string.h>
#include "gpio.h"
#include "config.h"
#include "flash.h"
#define PERIPHERAL_BUTTONS (0x01)
#define PERIPHERAL_USB (0x02)
#define PERIPHERAL_MOSFETS (0x04)
#define PERIPHERAL_RF (0x08)
/* When NRF is connected ESP should not be connected
 * so we can use the UART pins for other stuff.
 * In this case it is used to tell if we want to set
 * it in RF mode. */
Gpio gpio_RX_MODE = {
    .GPIOx = GPIOB,
    .pin = GPIO_PIN_7,
};

typedef struct _Config {
    uint8_t version;
    uint8_t rf_on;
    uint8_t rx_address;
    uint8_t tx_address;
    uint8_t peripherals;
} Config;
static bool config_save(Config *config);

const static Config *_config = (Config*)(0x08000000 + (CONFIG_PAGE * FLASH_PAGE_SIZE));
bool config_init(void)
{
    Config config;
    memcpy(&config, _config, sizeof(Config));
    if (config.version == 0xFF) {
        gpio_set_as_input(&gpio_RX_MODE, GPIO_PUPDR_PULLUP);
        config.version = 0;
        config.rf_on = gpio_read_pin(&gpio_RX_MODE) == false ? 1 : 0;
        config.rx_address = 0x01;
        config.tx_address = 0x01;
        config.peripherals = PERIPHERAL_USB | PERIPHERAL_RF | PERIPHERAL_BUTTONS | PERIPHERAL_MOSFETS;
        return config_save(&config);
    }

    return true;
}

bool config_rf_on(void)
{
    return _config->rf_on == 1;
}

bool config_set_tx_address(uint8_t address)
{
    Config config;
    memcpy(&config, _config, sizeof(Config));
    config.tx_address = address;
    return config_save(&config);
}

bool config_set_rx_address(uint8_t address)
{
    Config config;
    memcpy(&config, _config, sizeof(Config));
    config.rx_address = address;
    return config_save(&config);
}

uint8_t config_rx_address(void)
{
    return _config->rx_address;
}

uint8_t config_tx_address(void)
{
    return _config->tx_address;
}

bool config_usb_enabled(void)
{
    return _config->peripherals & PERIPHERAL_USB;

}

static bool config_save(Config *config)
{
    if (!flash_init())
        return false;

    flash_set_page(CONFIG_PAGE);
    flash_push_bytes((uint8_t*)config, sizeof(Config));
    return flash_execute();
}

