#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
extern "C" {
#endif
#include <stm32f0xx.h>
#include <stdbool.h>


/* If you don't like this API there is HAL shit API for U
 * or help me improve this shit since it is open source :) */

typedef enum _GPIO_PUPDR {
    GPIO_PUPDR_NONE = 0,
    GPIO_PUPDR_PULLUP = 1,
    GPIO_PUPDR_PULLDOWN = 2
} GPIO_PUPDR ;

typedef enum _GPIO_AFSEL {
    GPIO_AFSEL_AF0 = 0,
    GPIO_AFSEL_AF1 = 1,
    GPIO_AFSEL_AF2 = 2,
    GPIO_AFSEL_AF3 = 3,
    GPIO_AFSEL_AF4 = 4,
    GPIO_AFSEL_AF5 = 5,
    GPIO_AFSEL_AF6 = 6,
    GPIO_AFSEL_AF7 = 7
} GPIO_AFSEL;

typedef enum _GPIO_OSPEEDR {
    GPIO_OSPEEDR_LOW = 0,
    GPIO_OSPEEDR_MEDIUM = 1,
    GPIO_OSPEEDR_FAST = 2,
    GPIO_OSPEEDR_HIGH = 3
} GPIO_OSPEEDR;

typedef enum _GPIO_MODER {
    GPIO_MODER_INPUT = 0,
    GPIO_MODER_OUTPUT = 1,
    GPIO_MODER_ALTERNATE = 2,
    GPIO_MODER_ANALOG = 3
} GPIO_MODER;

typedef enum  _OTYPER {
    GPIO_OTYPER_PP = 0,
    GPIO_OTYPER_OD = 1
} GPIO_OTYPER;

typedef enum _GPIO_EDGE {
    GPIO_EDGE_FALLING = 0,
    GPIO_EDGE_RAISING = 1
} GPIO_EDGE;

typedef struct _Gpio {
    GPIO_TypeDef *GPIOx;
    uint16_t pin;
} Gpio;

bool gpio_set_as_input(Gpio *gpio, GPIO_PUPDR pul);
bool gpio_set_as_analog(Gpio *gpio);
bool gpio_set_as_output(Gpio *gpio, GPIO_OTYPER otyper, bool initial_state);
bool gpio_set_as_alternate_input(Gpio *gpio, GPIO_PUPDR pup, GPIO_AFSEL afsel);
bool gpio_set_as_alternate_output(Gpio *gpio, GPIO_OTYPER otyper, GPIO_AFSEL afsel, bool initial_state);
void gpio_enable_isr(Gpio *gpio, GPIO_EDGE edge);
void gpio_disable_isr(Gpio *gpio);
static inline bool gpio_read_pin(Gpio *gpio)
{
    return gpio->GPIOx->IDR & (gpio->pin);
}

static inline void gpio_set_pin(Gpio *gpio)
{
    gpio->GPIOx->BSRR |= (gpio->pin);
}

static inline void gpio_clear_pin(Gpio *gpio)
{
    gpio->GPIOx->BSRR |= (gpio->pin << 16);
}

static inline void gpio_write_pin(Gpio *gpio, bool set)
{
    if (set) gpio_set_pin(gpio); else gpio_clear_pin(gpio);
}

static inline void gpio_toggle_pin(Gpio *gpio)
{
    (gpio->GPIOx->ODR & gpio->pin) ? gpio_clear_pin(gpio) : gpio_set_pin(gpio);
}

#ifdef __cplusplus
}
#endif
#endif /*__gpio_H */

