#ifndef __FLASH_H__
#define __FLASH_H__
#include <stdint.h>
#include <stdbool.h>
#if defined(STM32F030x6) || defined(STM32F030x8) || defined(STM32F031x6) || defined(STM32F038xx) \
 || defined(STM32F051x8) || defined(STM32F042x6) || defined(STM32F048xx) || defined(STM32F058xx) || defined(STM32F070x6)
#define FLASH_PAGE_SIZE          0x400
#endif /* STM32F030x6 || STM32F030x8 || STM32F031x6 || STM32F051x8 || STM32F042x6 || STM32F048xx || STM32F058xx || STM32F070x6 */

#if defined(STM32F071xB) || defined(STM32F072xB) || defined(STM32F078xx) || defined(STM32F070xB) \
 || defined(STM32F091xC) || defined(STM32F098xx) || defined(STM32F030xC)
#define FLASH_PAGE_SIZE          0x800
#endif /* STM32F071xB || STM32F072xB || STM32F078xx || STM32F091xC || STM32F098xx || STM32F030xC */
#if defined(UNITTEST)
#define FLASH_PAGE_SIZE 0x400
#endif

bool flash_init(void);
bool flash_set_page(uint8_t page);
bool flash_push_bytes(const uint8_t *bytes, uint16_t size);
bool flash_execute(void);

#endif /* __FLASH_H__ */
