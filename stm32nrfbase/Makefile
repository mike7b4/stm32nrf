.PHONY: all clean

SHELL := /bin/bash
PATH := $(PATH):/development/toolchains/gcc-arm-none-eabi-6_2-2016q4/bin
CC=arm-none-eabi-gcc
LD=arm-none-eabi-gcc
AS=arm-none-eabi-as
OBJCOPY=arm-none-eabi-objcopy
OBJDUMP=arm-none-eabi-objdump
SIZE=arm-none-eabi-size

OBJECTS_STM32COOLBOARD=\
	../hw/rfcontroller2/rfcontroller.o \
	../hw/rfcontroller2/stm32f0xx_it.o \
	../startup/stm32f042/system_stm32f0xx.o \
	../startup/stm32f042/startup_stm32f042x6.o
	

OBJECTS_HAL_STM32F0XX=\
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_rcc.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_rcc_ex.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_pwr.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_gpio.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_dma.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_spi.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_adc.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_adc_ex.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_tim.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_tim_ex.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_pcd.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_pcd_ex.o \
	../Drivers/STM32F0xx_HAL_Driver/stm32f0xx_hal_cortex.o \
	../Middlewares/ST/STM32_USB_Device_Library/Core/usbd_core.o \
	../Middlewares/ST/STM32_USB_Device_Library/Core/usbd_ioreq.o \
	../Middlewares/ST/STM32_USB_Device_Library/Core/usbd_ctlreq.o \
	../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/usbd_cdc.o

SOURCES=\
	main.c \
	rtc.c \
	queue.c \
	event.c \
	gpio.c \
	mosfet.c \
	measure.c \
	config.c \
	tim.c \
	nrf.c \
	led.c \
	spi.c \
	adc.c \
	flash.c \
	buttons.c \
	stm_delay.c \
	command_table.c \
	nrf_custom.c \
	radio_protocol.c \
	../../libembedded/src/em_printf.c \
	../../libembedded/src/em_array.c \
	../../libembedded/src/em_queue.c \
	usbd_cdc_if.c \
	usb_device.c \
	usbd_conf.c \
	usbd_desc.c

INCLUDES_STM32F042=\
	-I../Drivers/STM32F0xx_HAL_Driver/ \
	-I../startup/stm32f042/

INCLUDES_STM32COOLBOARD=\
	-I../hw/rfcontroller2

# generic startup for all STM's
INCLUDES=\
	-I../Drivers/CMSIS/Include \
	-I../Middlewares/ST/STM32_USB_Device_Library/Core/ \
	-I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC \
	-I ../../libembedded/src \
	-I.

DEFINES=\
	-DSTM32F042x6 \
	-D__CORTEXM0 \
	-DUSE_HAL_DRIVER \
	-D__packed=-D"__attribute__((__packed__))"

LDFLAGS=\
	-mcpu=cortex-m0 -mthumb \
	-O3 \
	-ggdb \
	-Wl,--gc-sections \
	--specs=nosys.specs \
	--specs=nano.specs \
	-T../startup/stm32f042/STM32F042K6_FLASH.ld

LIBS=-lnosys

ASFLAGS=

CFLAGS=\
	$(DEFINES) \
	-Werror \
	-Wall \
	-Wpedantic \
	-ggdb -g3 \
	-Os \
	-mcpu=cortex-m0 \
	-mthumb \
	-std=gnu11 \
	-fmessage-length=0 \
	-ffunction-sections \
	-fdata-sections  \
	-MMD \
	$(INCLUDES) \
	$(INCLUDES_STM32COOLBOARD) \
	$(INCLUDES_STM32F042)

OBJECTS=$(SOURCES:.c=.o)


%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<
 
all: tests stm32coolboard

clean:
	rm -f *.o *.elf *.bin *.map *.hex *.d test_*


tests: test_flash
	./$<

test_flash: flash.c
	gcc -std=c11 -Wall -Werror -DUNITTEST $< -o $@

stm32coolboard: stm32coolboard.elf
	$(OBJCOPY) -Obinary $@.elf $@.bin
	$(OBJDUMP) -d $@.elf > $@.map
	$(SIZE) $@.elf

stm32coolboard.elf: $(OBJECTS) $(OBJECTS_HAL_STM32F0XX) $(OBJECTS_STM32COOLBOARD)
	$(LD) $(LDFLAGS) -o $@ $(OBJECTS) $(OBJECTS_STM32COOLBOARD) $(OBJECTS_HAL_STM32F0XX) $(LIBS)

.PHONY: tests all clean
