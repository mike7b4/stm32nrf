#include "event.h"
static EmQueue qevent;
void event_init(void)
{
    static EventObject pool[16];
    em_queue_init(&qevent, &pool, 16, sizeof(EventObject));
}

void event_push(uint8_t sender, uint8_t req)
{
    EventObject obj = {.sender = sender, .request = req };
    em_queue_push(&qevent, &obj);
}

bool event_pop(EventObject *obj)
{
	if (obj == NULL)
		return false;

	obj->sender = 0;
	obj->request = 0;
    if (!em_queue_pop(&qevent, obj))
        return false;

    return true;
}

