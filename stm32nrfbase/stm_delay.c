#include "gpio.h"
#include "hw_config.h"
#include "stm_delay.h"

void stm_msleep(uint16_t ms)
{
    __HAL_RCC_TIM17_CLK_ENABLE();
    /* set prescaler value to */
    TIM17->PSC = 48000 - 1;
    /* feed reload with ms * 2 */
    TIM17->ARR = (ms << 1);
    /* clear pending if there is any, just for safeness */
    TIM17->SR = 0;
    TIM17->DIER = TIM_DIER_UIE;
    NVIC_EnableIRQ(TIM17_IRQn);
    /* Enable single shot only */
    TIM17->CR1 = (TIM_CR1_CEN | TIM_CR1_OPM);
//    TIM17->EGR = TIM_EGR_UG;
    /* stay here until done */
    while ((TIM17->SR & 0x01) == 0) {
       // asm volatile("wfi");
    };

    /* not sure this is needed but... */
    TIM17->DIER = 0;
    TIM17->SR = 0;
    TIM17->CR1 = 0;
    __HAL_RCC_TIM17_CLK_DISABLE();
}

#if 0
void stm_usleep(uint16_t us)
{
    __HAL_RCC_TIM17_CLK_ENABLE();
    /* set prescaler value to */
    TIM17->PSC = 16000 - 1;
    /* feed reload with ms * 2 */
    TIM17->ARR = (us);
    /* clear pending if there is any, just for safeness */
    TIM17->SR = 0;
    TIM17->DIER = TIM_DIER_UIE;
    NVIC_EnableIRQ(TIM17_IRQn);
    /* Enable single shot only */
    TIM17->CR1 = (TIM_CR1_CEN | TIM_CR1_OPM);
//    TIM17->EGR = TIM_EGR_UG;
    /* stay here until done */
    while ((TIM17->SR & 0x01) == 0) {
       // asm volatile("wfi");
    };

    /* not sure this is needed but... */
    TIM17->DIER = 0;
    TIM17->SR = 0;
    TIM17->CR1 = 0;
    __HAL_RCC_TIM17_CLK_DISABLE();
}
#endif
void TIM17_IRQHandler(void)
{
    TIM17->DIER = 0;
}
