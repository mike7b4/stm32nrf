
#include <stdbool.h>
#include <i2c.h>

static I2C_HandleTypeDef i2c;
static bool i2c_initialized = false;
void i2c_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
    /**I2C1 GPIO Configuration    
    PA9     ------> I2C1_SCL
    PA10     ------> I2C1_SDA 
    */
    if (!i2c_initialized)
        return ;

    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF1_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    HAL_I2CEx_EnableFastModePlus(SYSCFG_CFGR1_I2C_FMP_PB6);
	__HAL_RCC_I2C1_CLK_ENABLE();
	i2c.Instance = I2C1;
	i2c.Init.Timing = 0x020B;
    i2c.Init.OwnAddress1 = 0;
    i2c.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	i2c.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	i2c.Init.OwnAddress2 = 0;
	i2c.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	i2c.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	i2c.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	HAL_I2C_Init(&i2c);
  	HAL_I2CEx_ConfigAnalogFilter(&i2c, I2C_ANALOGFILTER_ENABLE);

    i2c_initialized = true;
}

int i2c_write(uint8_t address, uint8_t *ptr, uint8_t length)
{
	int ok = HAL_I2C_Master_Transmit(&i2c, address, ptr, length, 5);

	return ok == HAL_OK ? length : -ok;
}

int i2c_read(uint8_t address, uint8_t *ptr, uint8_t length)
{
	int ok = HAL_I2C_Master_Receive(&i2c, address, ptr, length, 5);

	return ok == HAL_OK ? length : -ok;
}

