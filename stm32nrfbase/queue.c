#include "hw_config.h"
#include "queue.h"

void queue_init(Queue *p_queue, uint8_t *p_buffer, uint16_t size)
{
    p_queue->buffer = p_buffer;
    p_queue->size = size;
}

bool queue_pop(Queue *p_queue, uint8_t *byte)
{
    if (p_queue == NULL || p_queue->count == 0) {
        return false;
    }

    *byte = p_queue->buffer[p_queue->index];
	__disable_irq();
    p_queue->index =((p_queue->index+1) % p_queue->size);
    p_queue->count--;
	__enable_irq();

    return true;
}

bool queue_push(Queue *p_queue, uint8_t byte)
{
    if (p_queue == NULL || p_queue->count == p_queue->size) {
        return false;
    }

	__disable_irq();
    uint16_t ins = ((p_queue->count+p_queue->index) % p_queue->size);
    p_queue->buffer[ins] = byte;
    p_queue->count++;
	__enable_irq();
    return true;
}

