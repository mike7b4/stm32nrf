#ifndef QUEUE_H_
#define QUEUE_H_
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

typedef struct _Queue {
    uint8_t *buffer;
    volatile uint16_t index;
    volatile uint16_t count;
    uint16_t size;
} Queue;

void queue_init(Queue *queue, uint8_t *p_data, uint16_t size);
bool queue_push(Queue *queue, uint8_t byte);
bool queue_pop(Queue *queue, uint8_t *byte);

#endif /* end of include guard: QUEUE_H_ */

