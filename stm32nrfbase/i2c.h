#ifndef _I2C_H_
#define _I2C_H_
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_i2c.h"
#include "stm32f0xx_hal_gpio.h"
#include <stdint.h>

void i2c_init(void);
int i2c_write(uint8_t address, uint8_t *ptr, uint8_t length);
int i2c_read(uint8_t address, uint8_t *ptr, uint8_t length);

#endif /* end of I2C_H_ */
