#include "hw_config.h"
#if ENABLE_MAX31855==1
#include <string.h>
#include "gpio.h"
#include "max31855.h"

Gpio gpio_max31855_CSN = {
    .GPIOx = GPIOA,
    .pin = GPIO_PIN_1
};

void max31855_init(void)
{
    spi_init();
    gpio_set_as_output(&gpio_max31855_CSN, GPIO_OTYPER_PP, GPIO_AFSEL_AF0, true);
    spi_init();
}

bool max31855_read(MAX31855_RawData *raw)
{
    uint16_t data[2] = {0, 0};
    int res = HAL_SPI_ERROR_NONE;
    gpio_set_as_output(&gpio_max31855_CSN, GPIO_OTYPER_PP, GPIO_AFSEL_AF0, false);
    stm_msleep(500);
    res = HAL_SPI_Receive(&hspi1, (uint8_t *)&data[0], 2, 10);
    gpio_set_as_output(&gpio_max31855_CSN, GPIO_OTYPER_PP, GPIO_AFSEL_AF0, true);

    /* BIG ENDIAN first byte received is bit 31 */
    raw->data = data[0] << 16 | data[1];

    return res == HAL_OK ? true : false;
}

void max31855_raw_to_struct(MAX31855_RawData *raw, MAX31855_TempData *temp)
{
	temp->oc_bit = raw->data & 0x01;
	temp->scg_bit = raw->data & 0x02;
	temp->scv_bit = raw->data & 0x04;
    /* 11 bits */
	temp->internal_temp = ((raw->data & 0x7FF0 << 11));

    /* bit0 == thermo error check bit */
	temp->thermo_fault = raw->data & 0x00010000;

    /* thermo temp are bits 31:18 (bit 17 is reserved) */
	temp->thermo_temp = ((raw->data & 0x7FFC0000) >> 17);

    /* multiply by 0.25 to get celcius but since we hate float
     * we convert it to milli Celsius */
    temp->thermo_temp *= 250;
}

void max31855_print_diagnostic(MAX31855_RawData *raw, EMSTREAM *p_stream)
{
    if (raw == NULL || p_stream == NULL)
        return ;

    static MAX31855_TempData temp;
    max31855_raw_to_struct(raw, &temp);
    em_printf(p_stream, "Thermo status: %s\r\n", temp.thermo_fault ? "ERROR" : "OK");
    em_printf(p_stream, "Thermo temp: %d\r\n", temp.thermo_temp);
    em_printf(p_stream, "Internal temp: %d\r\n", temp.internal_temp);
    em_printf(p_stream, "Short to VCC: %s\r\n", temp.scv_bit ? "Yes" : "No");
    em_printf(p_stream, "Short to GND: %s\r\n", temp.scg_bit ? "Yes" : "No");
    em_printf(p_stream, "Open Circuit: %s\r\n", temp.oc_bit ? "Yes" : "No");
}
#endif
