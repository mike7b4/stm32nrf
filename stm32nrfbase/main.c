#include <string.h>
#include "hw_config.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#define TX_SIZE (64)
#include "spi.h"
#include "rtc.h"
#include "em_printf.h"
#include "em_array.h"
#include "measure.h"
#include "mosfet.h"
#include "led.h"
#include "gpio.h"
#include "event.h"
#include "nrf_custom.h"
#include "nrf.h"
#include "led.h"
#include "buttons.h"
#include "config.h"
#include "flash.h"
#include "tim.h"
#include "em_queue.h"
#include "nrf_custom.h"
#include "stm_delay.h"
#include "command_table.h"
#include "radio_protocol.h"
#if ENABLE_ADC==1
#include "adc.h"
#endif
#define USB_BUFFER_SIZE (64)

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
EmQueue tx_queue;
bool usb_try_send(uint8_t timeout);
extern USBD_HandleTypeDef hUsbDeviceFS;
bool cb_usb_put_byte(EMSTREAM *stream, uint8_t byte)
{
    if (!config_usb_enabled())
        return false;

    if (!em_queue_push(&tx_queue, &byte)) {
        usb_try_send(2);
        /* we can't do anything so we just drop it on the floor if fifo was not emptied */
        return em_queue_push(&tx_queue, &byte);
    }

    return true;
}

EmStream out = {
    .put_byte = cb_usb_put_byte
};

void HardFault_Handler(void)
{
    while(42) {
        asm volatile("nop");
    };
}

void UsageFault_Handler(void)
{
    while(42) {
        asm volatile("nop");
    };
}

void BusFault_Handler(int a)
{
    while(42) {
        asm volatile("nop");
    };
}

void stm32_reset(void)
{
    NVIC_SystemReset();
}

volatile bool statistic = false;
volatile EVENT sleepmode = EVENT_SLEEPMODE_STOP;
//volatile EVENT sleepmode = EVENT_SLEEPMODE_SLEEP;
int main(void)
{
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /* Reset of all peripherals, Initializes the 
     * Flash interface and the Systick. */
    HAL_Init();
    /* Configure the system clock */
    SystemClock_Config();
#if CONFIG_ERASE==1
    flash_init();
    flash_set_page(CONFIG_PAGE);
    flash_start();
#endif

    config_init();
    stm_msleep(2000);
    rtc_init();
    event_init();
    led_init();
    led_set(0, true);
    led_set(0, false);
    buttons_init();
    mosfet_init();
    adc_init();

    if (config_usb_enabled()) {
        static uint8_t _usbbuffer[USB_BUFFER_SIZE];
        em_queue_init(&tx_queue, _usbbuffer, USB_BUFFER_SIZE, sizeof(uint8_t));
        MX_USB_DEVICE_Init();
    }
    stm_msleep(1000);

    nrf_platform_setup();
    EventObject event = {0};
    while (42) {
        if (!event_pop(&event))
            goto sleep;

        if (em_commands_iterate_command(event.request, &out))
            continue;

        switch (event.request) {
        case EVENT_SLEEPMODE_SLEEP:
        case EVENT_SLEEPMODE_STOP:
            sleepmode = event.request;
            continue;
        case 0x7F:
            HAL_NVIC_SystemReset();
            break;
        case '1':
        case '2':
        case '3':
        case '4': {
            uint8_t mask = 1 << (event.request - '1');
            radio_protocol_send_set_output(nrf_get(), mask, mask, mask);
            em_printf(&out, "%c", event.request);
            break;
        }
        case 'b': {
            em_printf(&out, "%c", event.request);
            break;
        }
        case '.':
            /* send I am alive */
            em_printf(&out, ".");
            break ;
        case '$':
            em_printf(&out, "stm32coolboard says hello :)\r\n");
            break;
        }
sleep:
        if (!usb_try_send(1)) continue;
        if (sleepmode == EVENT_SLEEPMODE_STOP && !config_usb_enabled()) {
            HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        } else {
            HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
        }
    }

    /* Never reach. */
    return 42;
}

/**
* @brief This function handles System tick timer runs every 1ms.
*/
void SysTick_Handler(void)
{
    HAL_IncTick();

}
bool usb_cdc_tx_ready(USBD_HandleTypeDef *pdev)
{
    USBD_CDC_HandleTypeDef   *hcdc = (USBD_CDC_HandleTypeDef*) pdev->pClassData;

    if (hcdc != NULL)
        return hcdc->TxState == 0;

    return false;
}

/* run from main
 * FIXME ugly code
 * returns true if usb is not open or no more data to send. */
bool usb_try_send(uint8_t timeout)
{
    static uint8_t usbbuffer[64];

    if (!usb_is_open())
       return true;

    do {
        if (usb_cdc_tx_ready(&hUsbDeviceFS)) {

            int len = -1;
            do {
                len++;
            } while(len < sizeof(usbbuffer) && em_queue_pop(&tx_queue, &usbbuffer[len]) );

            if (len > 0) {
                CDC_Transmit_FS(usbbuffer, len);
            } else {
                // we are done
                return true;
            }
        }

        timeout--;
        if (!timeout)
            return false;

        // USB1.1 only one frame (64bytes)/ms
        stm_msleep(1);

    } while(1);

    return false;
}

