#ifndef __tim_H
#define __tim_H
#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"

bool tim_pwm_on(TIM_TypeDef *TIMx, uint8_t channel, uint32_t freq, uint8_t dutycycle);

#ifdef __cplusplus
}
#endif
#endif /*__ tim_H */

