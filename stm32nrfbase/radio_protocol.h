#ifndef RADIO_PROTOCOL_H_
#define RADIO_PROTOCOL_H_
#include "em_printf.h"
#include "nrf.h"
#define MSG_BODY_SIZE (6)
typedef enum _RPMsgType
{
	RP_MSG_PAIR_REQUEST = 0x7F,
    RP_MSG_SET_CLIENT_ID = 0x10,
    RP_MSG_SET_CLIENT_WAKEUP = 0x11,
    RP_MSG_SET_OUTPUT = 0x20,
    RP_MSG_SET_PWM = 0x21,
    RP_MSG_FLASH_SET_PAGE = 0x30,
    RP_MSG_FLASH_PUSH_DATA = 0x31,
    RP_MSG_FLASH_START = 0x32,
    RP_MSG_TEXT = 0x40,
} RPMsgType;

typedef struct _RPGeneric
{
	uint8_t msg;
	uint8_t sender_id;
    uint8_t body[MSG_BODY_SIZE];
} RPGeneric;

typedef struct _RP_Output {
	/* MSG_TYPE_SWITCHES */
	uint8_t msg;
	uint8_t sender_id;
    /* What switch(es) should be modified by switch_set below
     * in theory up to 8 switches is allowed. */
	uint8_t mask;
    /* What switch should be turned on/off up to 8 switches is supported. */
	uint8_t set;
    uint8_t toggle;
    uint8_t reserved[MSG_BODY_SIZE - 3];
} RP_Output;

typedef struct _RP_PWM {
	/* MSG_TYPE_SWITCHES */
	uint8_t msg;
	uint8_t sender_id;
    /* What switch(es) should be modified by switch_set below
     * up to 4 pwm is allowed. */
	uint8_t mask;
    /* What switch should be turned on/off up to 8 switches is supported. */
	uint8_t pwms[4];
    uint8_t reserved;
} RP_PWM;

typedef struct _RPText {
	/* MSG_TYPE_TEXT */
    uint8_t msg;
    uint8_t sender_id;
    uint8_t body[MSG_BODY_SIZE];
} RPText;

bool radio_protocol_init(Nrf *nrf, EMSTREAM *p_stream);
bool radio_protocol_send_set_output(Nrf *nrf, uint8_t switch_mask, uint8_t switches_set, uint8_t switches_toggle);
bool radio_protocol_send_set_pwm(Nrf *nrf, uint8_t index, uint8_t dutycycle);
bool radio_protocol_process_incoming(Nrf *nrf, EMSTREAM *out);
#endif /* end of include guard: RADIO_PROTOCOL_H_*/

