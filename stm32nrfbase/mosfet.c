#include "tim.h"
#include "mosfet.h"
#include "command_table.h"

static Gpio gpio_mosfets[] = {
    {
        .GPIOx = GPIOA,
        .pin = GPIO_PIN_7
    },
    {
        .GPIOx = GPIOB,
        .pin = GPIO_PIN_0
    },
    {
        .GPIOx = GPIOB,
        .pin = GPIO_PIN_1
    },
    {
        .GPIOx = GPIOA,
        .pin = GPIO_PIN_8
    }
};

static bool cb_set_fet_1(uint8_t cmd, const uint8_t *args, uint8_t length, EMSTREAM *sender)
{
    mosfet_toggle(0, 100);
    em_printf(sender, "1");
    return true;
}

static bool cb_set_fet_2(uint8_t cmd, const uint8_t *args, uint8_t length, EMSTREAM *sender)
{
    mosfet_toggle(1, 100);
    em_printf(sender, "2");
    return true;
}

void mosfet_init(void)
{
    mosfet_pwm_off(LOAD_SW1);
    mosfet_pwm_off(LOAD_SW2);
    mosfet_pwm_off(LOAD_SW3);
    mosfet_pwm_off(LOAD_SW4);

    em_command_register_callback('1', cb_set_fet_1);
    em_command_register_callback('2', cb_set_fet_2);
}

bool mosfet_set(uint8_t id, bool onoff)
{
    if (id > LOAD_SW4)
        return false;

    gpio_write_pin(&gpio_mosfets[id], onoff);
    return true;
}

bool mosfet_get(uint8_t id)
{
    switch (id) {
    case LOAD_SW1: {
        if ((TIM14->CR1 & TIM_CR1_CEN))
            return true;

        break;
    }
    case LOAD_SW2: {
        if ((TIM3->CR1 & TIM_CR1_CEN))
            return true;

        break;
    }
    case LOAD_SW3:
    case LOAD_SW4:
        break;
    default:
        return false;
    }

    return gpio_read_pin(&gpio_mosfets[id]);
}

bool mosfet_toggle(uint8_t id, uint8_t dutycycle)
{
    if (id > LOAD_SW4)
        return false;

    if (!mosfet_get(id)) {
        mosfet_pwm_on(id, dutycycle);
    } else {
        mosfet_pwm_off(id);
    }
    return true;
}

bool mosfet_pwm_off(uint8_t id)
{
    switch (id) {
    case LOAD_SW1:
        TIM14->CR1 = ~(TIM_CR1_CEN);
        __HAL_RCC_TIM14_CLK_DISABLE();
        gpio_set_as_output(&gpio_mosfets[LOAD_SW1], GPIO_OTYPER_PP, false);
        return true;
    case LOAD_SW2:
        TIM3->CR1 = ~(TIM_CR1_CEN);
        __HAL_RCC_TIM3_CLK_DISABLE();
        gpio_set_as_output(&gpio_mosfets[LOAD_SW2], GPIO_OTYPER_PP, false);
        return true;
    case LOAD_SW3:
        gpio_set_as_output(&gpio_mosfets[LOAD_SW3], GPIO_OTYPER_PP, false);
        return true;
    case LOAD_SW4:
        gpio_set_as_output(&gpio_mosfets[LOAD_SW4], GPIO_OTYPER_PP, false);
        return true;
     default:
        break;
    };

    return false;
}

static void mosfet_pwm_change(uint8_t id, TIM_TypeDef *p_tim, uint16_t channel, uint32_t af, uint8_t dutycycle)
{
    switch (dutycycle) {
    case 0:
       mosfet_pwm_off(id);
       break;
    case 100:
       /* 100 % means fully ON */
       mosfet_pwm_off(id);
       mosfet_set(id, true);
       break;
    default:
        tim_pwm_on(p_tim, channel, 6000, dutycycle);
        gpio_set_as_alternate_output(&gpio_mosfets[id], GPIO_OTYPER_PP, af, false);
    }

}

bool mosfet_pwm_on(uint8_t id, uint8_t dutycycle)
{
    dutycycle = 90;
    switch (id) {
    case LOAD_SW1:
        __HAL_RCC_TIM14_CLK_ENABLE();
        mosfet_pwm_change(id, TIM14, TIM_CHANNEL_1, GPIO_AFSEL_AF4, dutycycle);
        return true;
    case LOAD_SW2:
        __HAL_RCC_TIM3_CLK_ENABLE();
        mosfet_pwm_change(id, TIM3, TIM_CHANNEL_3, GPIO_AFSEL_AF1, dutycycle);
        return true;
    default:
        return mosfet_set(id, true);
    }

    return false;
}

void mosfet_send_state(EMSTREAM *p_stream)
{
	em_printf(p_stream, "m1=%d", mosfet_get(LOAD_SW1));
	em_printf(p_stream, "m2=%d", mosfet_get(LOAD_SW2));
	em_printf(p_stream, "m3=%d", mosfet_get(LOAD_SW3));
	em_printf(p_stream, "m4=%d", mosfet_get(LOAD_SW4));
}

