#ifndef NRF_H_
#define NRF_H_
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <list.h>
#include "em_printf.h"
#define NRF_REG_NOP (0xFF)
#define NRF_REG_CONFIG (0x00)
#define NRF_REG_EN_AA (0x01)
#define NRF_REG_EN_RXADDR (0x02)
#define NRF_REG_SETUP_AW (0x03)
#define NRF_REG_SETUP_RETR (0x04)
#define NRF_REG_RF_CH (0x05)
#define NRF_REG_RF_SETUP (0x06)
#define NRF_REG_STATUS (0x07)
#define NRF_REG_RX_ADDR_P0 (0x0A)
#define NRF_REG_TX_ADDR (0x10)
#define NRF_REG_RX_PW_P0 (0x11)
#define NRF_REG_FIFO_STATUS (0x17)
#define NRF_REG_FEATURE (0x1D)
#define NRF_R_REGISTER (0x00)
#define NRF_W_REGISTER (0x20)
#define NRF_R_RX_PAYLOAD (0x61)
#define NRF_W_TX_PAYLOAD (0xA0)
#define NRF_FLUSH_TX (0xE1)
#define NRF_FLUSH_RX (0xE2)
#define NRF_PIPE0 (0)
#define NRF_PIPE1 (1)
#define NRF_PIPE2 (2)
#define NRF_PIPE3 (3)
#define NRF_PIPE4 (4)
#define NRF_PIPE5 (5)
#define NRF_CONFIG_MASK_RX_DR (0x40)
#define NRF_CONFIG_MASK_TX_DS (0x20)
#define NRF_CONFIG_MASK_MAX_RT (0x10)
#define NRF_CONFIG_EN_CRC (0x08)
#define NRF_CONFIG_CRCO (0x04)
#define NRF_CONFIG_PWR_UP (0x02)
#define NRF_CONFIG_PRIM_RX (0x01)

#define NRF_RF_SETUP_PLL_LOCK (0x10)
#define NRF_RF_SETUP_CONT_WAVE (0x40)
#define NRF_RF_SETUP_DR_1Mbps ((0x00 << 5) | (0x00 << 3))
#define NRF_RF_SETUP_DR_2Mbps ((0x00 << 5) | (0x01 << 3))
#define NRF_RF_SETUP_DR_250Kbps ((0x01 << 5) | (0x00 << 3))
#define NRF_RF_SETUP_PWR_18dBm (0x00 << 1)
#define NRF_RF_SETUP_PWR_12dBm (0x01 << 1)
#define NRF_RF_SETUP_PWR_6dBm (0x02 << 1)
#define NRF_RF_SETUP_PWR_0dBm (0x03 << 1)


#define NRF_STATUS_RX_DR (0x40)
#define NRF_STATUS_TX_DR (0x20)
#define NRF_STATUS_MAX_RT (0x10)
#define NRF_TX_BUFFER_SIZE (34)
#define NRF_RX_BUFFER_SIZE (34)
#define NRF_RX_PIPE_SIZE (32)

typedef struct _Nrf Nrf;
typedef bool (*nrf_spi_transmit_receive)(uint8_t *txbuf, uint8_t *rxbuf, uint8_t length, void *p_data);
typedef void (*nrf_delay_us)(uint16_t us);
typedef void (*nrf_enable_radio)(bool enable);
struct _Nrf {
    uint8_t rx_buffer[NRF_RX_BUFFER_SIZE];
    uint8_t tx_buffer[NRF_TX_BUFFER_SIZE];
	uint8_t rx_pipe[NRF_RX_PIPE_SIZE];
    uint8_t status;
    uint8_t config;
    uint8_t rf_setup;
    uint8_t last_error;
    nrf_spi_transmit_receive spi_transmit_receive;
    nrf_delay_us delay_us;
    nrf_enable_radio enable_radio;
    void *p_data;
};

void nrf_init(Nrf *nrf);
bool nrf_set_register(Nrf *nrf, uint8_t reg, uint8_t value);
bool nrf_get_register(Nrf *nrf, uint8_t reg, uint8_t *value);
void nrf_interrupt_handler(Nrf *nrf);
bool nrf_power_down(Nrf *nrf);
bool nrf_enable_tx(Nrf *nrf);
bool nrf_enable_rx(Nrf *nrf);
bool nrf_set_channel(Nrf *nrf, uint8_t channel);
bool nrf_set_power_dBm(Nrf *nrf, uint8_t pwr);
bool nrf_set_datarate(Nrf *nrf, uint8_t rate);
bool nrf_transmit_data(Nrf *nrf, uint8_t *data, uint8_t length);
bool nrf_set_tx_address(Nrf *nrf, const uint8_t address[5]);
bool nrf_set_rx_address(Nrf *nrf, uint8_t pipe, const uint8_t address[5], uint8_t payload_width, bool enable_auto_ack);
bool nrf_tx_flush(Nrf *nrf);
bool nrf_rx_flush(Nrf *nrf);
void nrf_get_package(uint8_t *rx, uint8_t length);
uint8_t nrf_get_fifo_status(Nrf *nrf);
void nrf_dump_registers(Nrf *nrf, EMSTREAM *em);
#endif /* end of include guard: NRF_H_ */
