#ifndef __adc_H
#define __adc_H
#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"


extern ADC_HandleTypeDef hadc;

extern void Error_Handler(void);

void adc_init(void);


#ifdef __cplusplus
}
#endif
#endif /*__ adc_H */

