#ifndef _BUTTONS_H_
#define _BUTTONS_H_
#include <stdbool.h>
#include "em_printf.h"
#include "nrf.h"
bool buttons_init(void);
bool buttons_handler(Nrf *nrf, EMSTREAM *out);
#endif

