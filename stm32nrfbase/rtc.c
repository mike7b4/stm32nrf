#include "hw_config.h"
#include "rtc.h"
#include "event.h"

void rtc_init(void)
{
    __HAL_RCC_RTC_ENABLE();
    __HAL_RCC_RTC_CONFIG(RCC_RTCCLKSOURCE_LSI);
    /* Unlock RTC write */
    RTC->WPR = 0xCA;
    RTC->WPR = 0x53;
    RTC->ISR |= RTC_ISR_INIT;
    int timeout = 1200;
    while (!(RTC->ISR & RTC_ISR_INITF) && (timeout--) > 0) { asm volatile ("nop"); }

    if (!(RTC->ISR & RTC_ISR_INITF)) {
        asm volatile("bkpt");
    }

    RTC->PRER = 0x7F0137;
    /* 01:01:11 */
    RTC->TR = 0x010102;
    /* 2018-1970 = 48 == BCD == b0100 1000 = 0x48 */
    RTC->DR = 0x00483606;
    RTC->ISR &= ~(RTC_ISR_INIT);

    RTC->WPR = 0xFF;
    RTC->WPR = 0xCA;
    RTC->WPR = 0x53;

    RTC->CR &= RTC_CR_ALRAE;
    timeout = 2000;
    while (!(RTC->ISR & RTC_ISR_ALRAWF) && (timeout--) > 0) { asm volatile ("nop"); }
    if (!(RTC->ISR & RTC_ISR_ALRAWF)) {
        asm volatile("bkpt");
    }
    RTC->ALRMAR = 0x80808080;
    RTC->ISR &= ~(RTC_ISR_ALRAF);
    RTC->CR |= RTC_CR_ALRAE | RTC_CR_ALRAIE;
    RTC->WPR = 0xFF;

//    NVIC_EnableIRQ(RTC_IRQn);
    EXTI->IMR |= EXTI_IMR_MR17_Msk;
    EXTI->RTSR |= EXTI_RTSR_TR17_Msk;
}

volatile uint32_t rtc_sec = 0;
void RTC_IRQHandler(void)
{
    EXTI->PR = EXTI_PR_PR17_Msk;
    RTC->ISR &= ~(RTC_ISR_ALRAF);
    rtc_sec++;
//    if ((rtc_sec % 2) == 0)
//        event_push(SENDER_SYSTICK, EVENT_SLEEPMODE_SLEEP);
 //   else
  //      event_push(SENDER_SYSTICK, EVENT_SLEEPMODE_STOP);
}
