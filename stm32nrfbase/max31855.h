#ifndef _MAX31855_H__
#define _MAX31855_H__
#include <stdbool.h>
#include <stdint.h>
#include "em_printf.h"
#include "hw_config.h"
#include "spi.h"
typedef struct _MAX31855_RawData {
    uint32_t data;
} MAX31855_RawData;

typedef struct _MAX31855_TempData {
    int32_t thermo_temp;
    int32_t internal_temp;
    bool scv_bit;
    bool scg_bit;
    bool oc_bit;
    bool thermo_fault;
} MAX31855_TempData;

void max31855_init(void);
bool max31855_read(MAX31855_RawData *raw);
void max31855_print_diagnostic(MAX31855_RawData *raw, EMSTREAM *p_stream);
void max31855_raw_to_struct(MAX31855_RawData *raw, MAX31855_TempData *temp);
#endif /* end of _MAX31855_H__ */
